package com.example.demo.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

@Controller
@RequestMapping(value = "/millionaire")
@CrossOrigin(origins = "*",methods = {RequestMethod.GET})
public class Millionaire {

    @RequestMapping(value = "/question/{id}", method = RequestMethod.GET)
    public ResponseEntity<DocumentDto> returnQuestion(@PathVariable("id") Integer id){

        String []q = getQuestion(id);

        if(q == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        final DocumentDto documentDto = new DocumentDto(q[1], answersToTable(q),Integer.parseInt(q[6]));

        if(documentDto != null){
            return new ResponseEntity<>(documentDto, HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //losowanie numeru pytania
    private int losujPytanie(){
        int i;
        Random random = new Random();
        i=random.nextInt(10);
        return i+1;
    }

    //czytanie pliku csv
    private String[] getQuestion(int i) {

        String []files = new String[]{"","src\\1.csv","src\\2.csv","src\\3.csv","src\\4.csv","src\\5.csv",
                "src\\6.csv", "src\\7.csv", "src\\8.csv","src\\9.csv","src\\10.csv","src\\11.csv","src\\12.csv"};

        Integer lineNumber=0 ;
        Integer wylosowanyNumer = losujPytanie();

        BufferedReader bufferedReader = null;
        String line = "";
        String []question = new String[7];

        try {
            bufferedReader = new BufferedReader(new FileReader(files[i]));
            while ((line = bufferedReader.readLine()) != null) {

                if(lineNumber==wylosowanyNumer){
                    String row[] = line.split(",");
                    question=row;
                    break;
                }
                lineNumber++;
            }
        }catch(Exception e){
            return null;
            }
        finally {
            try {
                if(bufferedReader!=null)
                    bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(question[0]!=null){
            return question;
        }
        else return null;

    }

    //odpowiedzi na pytanie są zapisane do jednej tablicy
    private String[] answersToTable(String []question){
        String []table = new String[4];
        table[0]=question[2];
        table[1]=question[3];
        table[2]=question[4];
        table[3]=question[5];

        return table;
    }
}
