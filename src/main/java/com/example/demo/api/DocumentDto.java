package com.example.demo.api;

public class DocumentDto {
    private String question;
    private String[] answers;
    private Integer correctAnswer;

    public DocumentDto(){}

    public DocumentDto(String question, String []answers, Integer correctAnswer){
        this.question = question;
        this.answers = answers;
        this.correctAnswer = correctAnswer;
    }

    public String getQuestion() {
        return question;
    }

    public String[] getAnswers() {
        return answers;
    }

    public Integer getCorrectAnswer() {
        return correctAnswer;
    }
}


